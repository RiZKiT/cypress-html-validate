/* load cypress results and create a flat object with full spec name -> spec
 * (file is created by cypress) */
const cypressResults = require("./results.json");

const cypressSuites = Object.values(cypressResults.suites);
const cypressSpecs = cypressSuites.reduce((out, suite) => {
	for (const spec of suite.tests) {
		out[spec.fullTitle] = spec;
	}
	return out;
}, {});

describe("cypress-html-validate", () => {
	let cypressSpec;

	beforeAll(() => {
		/* for each spec load the corresponding result from cypress */
		/* eslint-disable-next-line jest/no-jasmine-globals */
		jasmine.getEnv().addReporter({
			specStarted: function (spec) {
				cypressSpec = cypressSpecs[spec.fullName];
			},
		});
	});

	beforeEach(() => {
		/* eslint-disable-next-line jest/no-standalone-expect */
		expect(cypressSpec).toBeDefined();
	});

	it("should not report error when a valid page is loaded", () => {
		expect.assertions(2);
		expect(cypressSpec.result).toEqual("passed");
	});

	it("should report error when invalid page is loaded", () => {
		expect.assertions(3);
		expect(cypressSpec.result).toEqual("failed");
		expect(cypressSpec.err.message).toEqual(
			"1 error, 0 excluded: expected 1 to equal 0"
		);
	});

	it("should report error when an action causes document to be invalid", () => {
		expect.assertions(3);
		expect(cypressSpec.result).toEqual("failed");
		expect(cypressSpec.err.message).toEqual(
			"2 errors, 0 excluded: expected 2 to equal 0"
		);
	});

	it("should not report excluded errors", () => {
		expect.assertions(2);
		expect(cypressSpec.result).toEqual("passed");
	});

	it("should not report errors outside included elements", () => {
		expect.assertions(2);
		expect(cypressSpec.result).toEqual("passed");
	});
});

# cypress-html-validate changelog

## [1.2.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.2.0...v1.2.1) (2020-05-29)

### Bug Fixes

- disable `attribute-empty-style` ([a2fb02b](https://gitlab.com/html-validate/cypress-html-validate/commit/a2fb02bc3832622987f3663e6f444b9f105148b8)), closes [#1](https://gitlab.com/html-validate/cypress-html-validate/issues/1)

# [1.2.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.1.1...v1.2.0) (2020-02-26)

### Features

- support excluding/including errors via selectors ([32ec9dc](https://gitlab.com/html-validate/cypress-html-validate/commit/32ec9dc87db9c86909edb26961111117f7ff1e1d))

## [1.1.1](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.1.0...v1.1.1) (2020-02-17)

### Bug Fixes

- **config:** use new void rules ([1b585d3](https://gitlab.com/html-validate/cypress-html-validate/commit/1b585d3110533981201c88b570649796e82c712a))

# [1.1.0](https://gitlab.com/html-validate/cypress-html-validate/compare/v1.0.0...v1.1.0) (2020-01-26)

### Features

- lookup dom elements if selector is present ([4a25877](https://gitlab.com/html-validate/cypress-html-validate/commit/4a25877f44038e506092340d438deac8b68c83c0))

# 1.0.0 (2020-01-24)

### Features

- initial version ([c682dec](https://gitlab.com/html-validate/cypress-html-validate/commit/c682decb0c05f4a99dccc6b2ccc01bdc2346977d))

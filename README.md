# Cypress HTML-Validate plugin

Validates HTML using `html-validate`.
It automatically fetches the active source markup from the browser and validates, failing the test if any validation errors is encountered.

## Usage

### In `cypress/plugins/index.js`:

```js
const htmlvalidate = require("cypress-html-validate/dist/plugin");

module.exports = (on) => {
  htmlvalidate.install(on);
};
```

### In `cypress/support/index.js`:

```js
import "cypress-html-validate/dist/commands";
```

### In specs:

```js
it("should be valid", () => {
  cy.visit("/my-page.html");
  cy.htmlvalidate();
});
```

## Configuration

`html-validate` and the plugin can configured in `cypress/plugins/index.js`:

```js
/* html-validate configuration */
const config = {
  rules: {
    foo: "error",
  },
};
/* plugin options */
const options = {
  exclude: [],
  include: [],
};
htmlvalidate.install(on, config, options);
```

## Options

### `exclude: string[]`

A list of selectors to ignore errors from.
Any errors from the elements or any descendant will be ignored.

### `include: []`

A list of selectors to include errors from.
If this is set to non-empty array only errors from elements or any descendants will be included.

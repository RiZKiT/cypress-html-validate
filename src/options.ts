export interface CypressHtmlValidateOptions {
	exclude: string[];
	include: string[];
}

const options: CypressHtmlValidateOptions = {
	exclude: [],
	include: [],
};

export default options;

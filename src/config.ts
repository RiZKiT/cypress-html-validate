import { ConfigData } from "html-validate";

const config: ConfigData = {
	root: true,

	extends: ["html-validate:recommended", "html-validate:document"],

	rules: {
		/* doctype is not passed when requesting source */
		"missing-doctype": "off",

		/* some frameworks (such as jQuery) often uses inline style, e.g. for
		 * showing/hiding elements */
		"no-inline-style": "off",

		/* scripts will often add markup with trailing whitespace */
		"no-trailing-whitespace": "off",

		/* browser normalizes boolean attributes */
		"attribute-boolean-style": "off",
		"attribute-empty-style": "off",
	},
};

export default config;

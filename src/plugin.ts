import { HtmlValidate, ConfigData } from "html-validate";
import deepmerge from "deepmerge";
import defaultConfig from "./config";
import options, { CypressHtmlValidateOptions } from "./options";

export function install(
	on: (action: string, arg: Record<string, Function>) => void,
	userConfig?: ConfigData,
	userOptions?: CypressHtmlValidateOptions
): void {
	const config = deepmerge(defaultConfig, userConfig || {});
	const htmlvalidate = new HtmlValidate(config);

	Object.assign(options, userOptions);

	if (!Array.isArray(options.exclude)) {
		throw new Error(
			`Invalid cypress-html-validate configuration, "exclude" must be array, got ${typeof options.exclude}`
		);
	}

	if (!Array.isArray(options.include)) {
		throw new Error(
			`Invalid cypress-html-validate configuration, "include" must be array, got ${typeof options.include}`
		);
	}

	on("task", {
		htmlvalidate(source: string) {
			return htmlvalidate.validateString(source);
		},
		htmlvalidateOptions() {
			return options;
		},
	});
}

import { Message, Report } from "html-validate/build/reporter";
import { CypressHtmlValidateOptions } from "./options";

/* eslint-disable-next-line @typescript-eslint/no-namespace */
export declare namespace Cypress {
	interface Chainable<Subject> {
		htmlvalidate(): void;
	}
}

interface ElementMessage extends Message {
	element?: Element;
}

function getPageSource(document: Document): string {
	const matchCypress = new RegExp(
		'<script type="text/javascript">.*?var Cypress = window.Cypress.*?</script>'
	);
	const root = document.documentElement;
	const source = root.outerHTML.replace(matchCypress, "");
	return source;
}

function logError(error: Message, $el?: JQuery<HTMLElement>): void {
	Cypress.log({
		$el,
		name: error.ruleId,
		consoleProps: () => error,
		message: [error.message],
	});
}

function findElements(
	messages: Message[],
	document: Document
): ElementMessage[] {
	return messages.map((message) => {
		return {
			...message,
			element: message.selector
				? document.querySelector(message.selector)
				: null,
		};
	});
}

function matchSelectors(
	message: ElementMessage,
	selector: Element[][]
): boolean {
	const target = message.element;
	return selector.some((elements) => {
		return elements.some((inner) => inner.contains(target));
	});
}

function filterMessages(
	messages: ElementMessage[],
	document: Document,
	options: CypressHtmlValidateOptions
): ElementMessage[] {
	const exclude = options.exclude.map((selector) =>
		Array.from(document.querySelectorAll(selector))
	);
	const include = options.include.map((selector) =>
		Array.from(document.querySelectorAll(selector))
	);
	return messages.filter((message) => {
		/* messages without an element cannot be filtered, assumed to be relevant */
		if (!message.element) {
			return true;
		}

		/* drop message if it is matched against excluded selector */
		if (exclude.length > 0 && matchSelectors(message, exclude)) {
			return false;
		}

		/* drop message if none of include selectors matches */
		if (include.length > 0) {
			return matchSelectors(message, include);
		}

		return true;
	});
}

function htmlvalidate(): void {
	Cypress.log({
		name: "html-validate",
		message: ["Validating document"],
	});

	cy.task("htmlvalidateOptions", null, { log: false }).then(
		(options: CypressHtmlValidateOptions) => {
			cy.document({ log: false })
				.then((document) => {
					const source = getPageSource(document);
					return cy
						.task("htmlvalidate", source, { log: false })
						.then((report: Report) => {
							if (report.valid) {
								return cy.wrap([0, 0], { log: false });
							} else {
								const messages = findElements(
									report.results[0].messages,
									document
								);
								const filtered = filterMessages(messages, document, options);
								cy.wrap(filtered, { log: false }).each(
									(error: ElementMessage) => {
										logError(error);
									}
								);
								return cy.wrap([messages.length, filtered.length], {
									log: false,
								});
							}
						});
				})
				.then(([messages, filtered]) => {
					const s = filtered !== 1 ? "s" : "";
					const excluded = messages - filtered;
					assert.equal(
						filtered,
						0,
						`${filtered} error${s}, ${excluded} excluded`
					);
				});
		}
	);
}

Cypress.Commands.add("htmlvalidate", htmlvalidate);

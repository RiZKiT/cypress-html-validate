context("cypress-html-validate", () => {
	it("should not report error when a valid page is loaded", () => {
		cy.visit("/valid.html");
		cy.htmlvalidate();
	});

	it("should report error when invalid page is loaded", () => {
		cy.visit("/invalid.html");
		cy.htmlvalidate();
	});

	it("should report error when an action causes document to be invalid", () => {
		cy.visit("/valid.html");
		cy.htmlvalidate();
		cy.get("#insert-invalid").click();
		cy.htmlvalidate();
	});

	it("should not report excluded errors", () => {
		cy.visit("/excluded.html");
		cy.htmlvalidate();
	});

	it("should not report errors outside included elements", () => {
		cy.visit("/included.html");
		cy.htmlvalidate();
	});
});

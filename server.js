const express = require("express");
const serveStatic = require("serve-static");

const app = express();
app.use(serveStatic("pages"));
app.listen(8080, "localhost");

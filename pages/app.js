(function () {
	const invalidContent =
		"<button><p>Invalid permitted permitted content</p></button>";
	const insertInvalid = document.querySelector("#insert-invalid");
	const containerInvalid = document.querySelector("#container-invalid");

	if (insertInvalid) {
		insertInvalid.addEventListener("click", () => {
			containerInvalid.innerHTML = invalidContent;
		});
	}
})();
